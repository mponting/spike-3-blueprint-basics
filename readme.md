| **Spike ID**        | B3                                                              | **Time Needed** | 3 hours |
|---------------------|-----------------------------------------------------------------|-----------------|---------|
| **Title**           | Base Spike 3 � Unreal Blueprint Basics                          |
| **Personnel**       | Mathew Ponting (blueprogrammer)                                 |
| **Repository Link** | <https://bitbucket.org/blueprogrammer/spike-3-blueprint-basics> |

Introduction
============

In this spike, the basics of the Unreal visual programming language will
be covered. This aims to let you have an understanding of how blueprints
works and how you can use them to achieve a desired goal.

Goal
====

The aim of this spike is to learn the basics of Unreal Engine Blueprint
Programming. This will be done by using the Unreal Blueprint Quick Start
guide. Then using the knowledge gained from that quick start, adapt it
over to the FPS Template and construct a FPS level that uses the launch
pads.

TRT (Technology, Resources and Tools)
=====================================

| **Resource**                                                                                               | **Needed/Recommended** |
|------------------------------------------------------------------------------------------------------------|------------------------|
| Unreal Engine 4                                                                                            | Needed                 |
| Blueprint Quick Start - <https://docs.unrealengine.com/latest/INT/Engine/Blueprints/QuickStart/index.html> | Needed                 |

Tasks
=====

1.  Create a new blueprint FPS template. Create a new actor with a cube
    mesh, box collider and arrow components and call it platform.

2.  Click Add Script/Blueprint for the platform and create it exactly
    the same as the Quick Start Platform.

3.  When getting the launch direction however, get the forward vector of
    the arrow component instead of the platform. This way you can adjust
    the arrow to the direction you want.

4.  Make sure the Launch Character node you set has both the XY and Z
    Overrides are enabled.

5.  Create an int variable called LaunchVelocity and make it public
    and editable.

6.  Multiply the forward vector of the arrow by the Launch Velocity and
    Make the Launch Character Velocity equal to that value.

7.  Add a Play Sound 2D node and add a sound of your choice.

8.  Save and build it and test in the FPS level.

9.  Create a new level then create multiple platforms and place multiple
    launch pads on the platforms. Play around with the launch velocity
    and arrow direction until you are happy with the launch positioning.

Discovered
==========

-   Blueprint is a visual programming language for the Unreal Engine and
    can work with C++.

-   Using an Arrow Component, you can identify if something needs to go
    in a certain direction.

-   Blueprint is handy if you quickly want to make something and
    prototype a game mechanic.

-   You can inherit anything such as a C++ class or another blueprint
    class to create a blueprint.

Recommendations
===============

-   Make sure all nodes connect together, if 2 are not connected, the
    code will not always work.

-   If you make a collider, make sure the collider can reach the objects
    that are going to collide with.

-   Group any nodes that are connected together that function together
    with commenting.

-   When updating the location, rotation or scale of an object in a
    Tick, include the Delta Time supplied with the Event Tick Node.

